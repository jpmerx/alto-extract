<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:loc="http://www.loc.gov/standards/alto/ns-v4#">
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

<!--    <xsl:template match="loc:TextLine[@TAGREFS != 'LT375']"/>-->
    <xsl:template match="loc:TextLine[@TAGREFS='true']"/>

</xsl:stylesheet>
