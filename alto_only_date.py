#!/usr/bin/python

from lxml.etree import parse
from lxml import etree
from sys import argv


class AltoDate:
    alto_ns = {'default': 'http://www.loc.gov/standards/alto/ns-v4#',
               'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}

    def __init__(self, alto_file):
        self.dom = parse(alto_file)

    def date_id(self):
        return self.dom.xpath('//default:Tags/default:OtherTag[@LABEL="Date"]/@ID', namespaces=self.alto_ns)[0]

    def text_lines(self):
        return self.dom.xpath('//default:TextLine', namespaces=self.alto_ns)

    @staticmethod
    def text_line_tag(text_line):
        tagrefs = text_line.xpath('@TAGREFS')
        return tagrefs[0] if tagrefs else None

    @staticmethod
    def text_line_id(text_line):
        return text_line.xpath('@ID')[0]

    def text_line_content(self, text_line):
        return text_line.xpath('default:String/@CONTENT', namespaces=self.alto_ns)[0]

    def only_dates(self):
        for text_line in self.text_lines():
            if self.text_line_tag(text_line) != self.date_id():
                text_line.getparent().remove(text_line)
        return etree.tostring(self.dom, xml_declaration=True, pretty_print=True, encoding='UTF-8')


def main(in_file, out_file):
    # See tutorial https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    # for more elaborate arguments management

    print(f"In file: {in_file}, out file: {out_file}")
    with open(out_file, "wb") as f:
        f.write(AltoDate(in_file).only_dates())


if __name__ == '__main__':
    main(argv[1], argv[2])
