#!/usr/bin/env bash

INFILES="/home/jpmerx/Documents/escriptorium/test5JF-01-date-zone/*.xml"
XML=".xml"

source /home/jpmerx/.virtualenvs/alto-extract/bin/activate

for i in ${INFILES}; do
  # Need to delete previous created files
  python alto_only_date.py "$i" "${i%${XML}}_date${XML}"
done