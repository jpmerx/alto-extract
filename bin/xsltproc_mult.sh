#!/usr/bin/env bash

INFILES="/home/jpmerx/Documents/escriptorium/test5JF-01-date-zone/*.xml"
XSLT="/home/jpmerx/Documents/escriptorium/xslt/date.xslt"
XML=".xml"

for i in ${INFILES}; do
  # Need to delete previous created files
  /usr/bin/xsltproc -o "${i%${XML}}_date${XML}" $XSLT "$i"
done