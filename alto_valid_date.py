from genauto.validation import is_script_date
from lxml.etree import parse


class AltoDate:
    alto_ns = {'default': 'http://www.loc.gov/standards/alto/ns-v4#',
               'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}

    def __init__(self, alto_file):
        self.dom = parse(alto_file)

    def date_id(self):
        return self.dom.xpath('//default:Tags/default:OtherTag[@LABEL="Date"]/@ID', namespaces=self.alto_ns)[0]

    def text_lines(self):
        return self.dom.xpath('//default:TextLine', namespaces=self.alto_ns)

    @staticmethod
    def text_line_tag(text_line):
        return text_line.xpath('@TAGREFS')[0]

    @staticmethod
    def text_line_id(text_line):
        return text_line.xpath('@ID')[0]

    def text_line_content(self, text_line):
        return text_line.xpath('default:String/@CONTENT', namespaces=self.alto_ns)[0]


def main():
    alto_date = AltoDate('//xml/archives_4_E_000504_000026_0056_date.xml')

    for text_line in alto_date.text_lines():
        if alto_date.text_line_tag(text_line) != alto_date.date_id():
            print(f"Text line {alto_date.text_line_id(text_line)} is not tagged as date!")
        elif not is_script_date(alto_date.text_line_content(text_line)):
            print(f"Text line {alto_date.text_line_id(text_line)} doesn't have a valid transcription")


if __name__ == '__main__':
    main()
