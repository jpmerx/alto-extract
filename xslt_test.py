from lxml.etree import parse, XSLT, tostring


def main():
    dom = parse('/home/jpmerx/dev/alto-extract/xml/test-01.xml')
    xslt = parse('/home/jpmerx/dev/alto-extract/xslt/date.xslt')
    transform = XSLT(xslt)
    newdom = transform(dom)
    print(tostring(newdom, pretty_print=True))


if __name__ == '__main__':
    main()
