from lxml.etree import parse


def main():
    dom = parse('/home/jpmerx/dev/alto-extract/xml/test-01.xml')

    # As XML elements don't have namespace attributes, a default one has to be defined and
    # used to prefix elements in the XML file. See:
    # https://stackoverflow.com/questions/8053568/how-do-i-use-empty-namespaces-in-an-lxml-xpath-query
    elements = dom.xpath('//default:TextLine', namespaces={'default': 'http://www.loc.gov/standards/alto/ns-v4#',
                                            'xsi': 'http://www.w3.org/2001/XMLSchema-instance'})
    print(f"Found {len(elements)} element(s)!")


if __name__ == '__main__':
    main()
